<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Plugin strings are defined here.
 *
 * @package     local_morenotifications
 * @category    string
 * @copyright   2020 Daniel Neis Araujo <daniel@adapta.online>
 * @license     https://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['indicator:coursesending'] = 'Courses with end date near';
$string['indicator:coursesending_help'] = 'Courses with end date near';
$string['indicator:coursesstarting'] = 'Courses with start date near';
$string['indicator:coursesstarting_help'] = 'Courses with start date near';
$string['pluginname'] = 'More Notifications';
$string['target:upcomingcoursesending'] = 'Upcoming courses ending';
$string['target:upcomingcoursesending_help'] = 'Upcoming courses ending';
$string['target:upcomingcoursesstarting'] = 'Upcoming courses starting';
$string['target:upcomingcoursesstarting_help'] = 'Upcoming courses starting';
